package ru.otus.homework.security;

public class Constants
{
    public static final String USERNAME_PARAMETER = "jUserLogin";
    public static final String PASSWORD_PARAMETER = "jUserPassword";
    public static final String FAILURE_URL = "/login?error=true";
}
