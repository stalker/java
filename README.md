# java-2018-11-VSkurikhin
## "Разработчик на Spring Framework"

#### (C) 2018-12
##### @Author Виктор Скурихин (Victor Skurikhin)

### HomeWork-12
 * [ ] В CRUD Web-приложение добавить механизм аутентификации
 * [ ] В существующее CRUD-приложение добавить мехнизм Form-based аутентификации.
 * [ ] UsersServices реализовать самостоятельно.
