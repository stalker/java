package su.svn.javaTheCompleteReference.boxdemo5;

// This program uses a parameterized method.
// В этой программе применяется метод с параметрами

class Box {
    double width;
    double height;
    double depth;

    // compute and return volume
    // рассчитать и возвратить объем
    double volume() {
        return width * height * depth;
    }

    // sets dimensions of box
    // установить размеры параллелепипеда
    void setDim(double w, double h, double d) {
        width = w;
        height = h;
        depth = d;
    }
}

class BoxDemo5 {
    public static void main(String args[]) {
        Box mybox1 = new Box();
        Box mybox2 = new Box();
        double vol;

        // initialize each box
        // инициализировать каждый экземпляр класса box
        mybox1.setDim(10, 20, 15);
        mybox2.setDim(3, 6, 9);

        // get volume of first box
        // получить объем первого параллелепипеда
        vol = mybox1.volume();
        System.out.println("Volume is " + vol);

        // get volume of second box
        // получить объем второго параллелепипеда
        vol = mybox2.volume();
        System.out.println("Volume is " + vol);
    }
}

