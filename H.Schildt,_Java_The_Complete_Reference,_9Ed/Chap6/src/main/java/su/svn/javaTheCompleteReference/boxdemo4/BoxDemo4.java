package su.svn.javaTheCompleteReference.boxdemo4;

// Now, volume() returns the volume of a box.
// Теперь метод volume() возвращает объем параллелепипеда.

class Box {
    double width;
    double height;
    double depth;

    // compute and return volume
    // рассчитать и возвратить объем
    double volume() {
        return width * height * depth;
    }
}

class BoxDemo4 {
    public static void main(String args[]) {
        Box mybox1 = new Box();
        Box mybox2 = new Box();
        double vol;

        // assign values to mybox1's instance variables
        // присвоить значения переменным экземпляра mybox1
        mybox1.width = 10;
        mybox1.height = 20;
        mybox1.depth = 15;

        // assign different values to mybox2's instance variables
        // присвоить другие значения переменным экземпляра mybox2
        mybox2.width = 3;
        mybox2.height = 6;
        mybox2.depth = 9;

        // get volume of first box
        // получить объем первого параллелепипеда
        vol = mybox1.volume();
        System.out.println("Volume is " + vol);

        // get volume of second box
        // получить объем второго параллелепипеда
        vol = mybox2.volume();
        System.out.println("Volume is " + vol);
    }
}
