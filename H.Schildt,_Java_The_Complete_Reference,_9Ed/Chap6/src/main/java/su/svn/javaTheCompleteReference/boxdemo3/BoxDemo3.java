// This program includes a method inside the box class.
// В этой программе применяетсяме тод, введенный в класс Box.

package su.svn.javaTheCompleteReference.boxdemo3;

class Box {
    double width;
    double height;
    double depth;

    // display volume of a box
    // вывести объем параллелепипеда
    void volume() {
        System.out.print("Volume is ");
        System.out.println(width * height * depth);
    }
}

class BoxDemo3 {
    public static void main(String args[]) {
        Box mybox1 = new Box();
        Box mybox2 = new Box();

        // assign values to mybox1's instance variables
        // присвоить значение переменным экземпляра mybox1
        mybox1.width = 10;
        mybox1.height = 20;
        mybox1.depth = 15;

        // assign different values to mybox2's instance variables
        // присвоить другие значения переменным экземпляра mybox2
        mybox2.width = 3;
        mybox2.height = 6;
        mybox2.depth = 9;

        // display volume of first box
        // рассчитать объем первого параллелепипеда
        mybox1.volume();

        // display volume of second box
        // рассчитать объем второго параллелепипеда
        mybox2.volume();
    }
}
