/*
 * A program that uses the su.svn.javaTheCompleteReference.boxdemo2.Box class.
 * Call this file su.svn.javaTheCompleteReference.boxdemo2.BoxDemo2.java
 * Программа, использующая класс su.svn.javaTheCompleteReference.boxdemo2.Box
 * Прис воить исх одному файлу имя su.svn.javaTheCompleteReference.boxdemo2.BoxDemo2.java.
 */

package su.svn.javaTheCompleteReference.boxdemo2;

class Box {
    double width;
    double height;
    double depth;
}

// This program declares two su.svn.javaTheCompleteReference.boxdemo2.Box objects.
// В этой программе объявляются два объекта класса su.svn.javaTheCompleteReference.boxdemo2.Box
class BoxDemo2 {
    public static void main(String args[]) {
        Box mybox1 = new Box();
        Box mybox2 = new Box();
        double vol;

        // assign values to mybox1's instance variables
        // присвоить значение переменным экземпляра mybox1
        mybox1.width = 10;
        mybox1.height = 20;
        mybox1.depth = 15;

        // assign different values to mybox2's instance variables
        // присвоить другие значения переменным экземпляра mybox2
        mybox2.width = 3;
        mybox2.height = 6;
        mybox2.depth = 9;

        // compute volume of first box
        // рассчитать объем первого параллелепипеда
        vol = mybox1.width * mybox1.height * mybox1.depth;
        System.out.println("Volume is " + vol);

        // compute volume of second box
        // рассчитать объем второго параллелепипеда
        vol = mybox2.width * mybox2.height * mybox2.depth;
        System.out.println("Volume is " + vol);
    }
}
