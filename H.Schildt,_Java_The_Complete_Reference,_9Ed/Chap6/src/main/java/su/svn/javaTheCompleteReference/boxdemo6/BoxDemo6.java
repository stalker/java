package su.svn.javaTheCompleteReference.boxdemo6;

/*
 * Here, Box uses a constructor to initialize the
 * dimensions of a box.
 * В данном примере программы для инициализации размеров
 * параллелепипеда в классе Box применяется конструктор.
 */

class Box {
    double width;
    double height;
    double depth;

    // This is the constructor for Box.
    // Это конструктор класса Box.
    Box() {
        System.out.println("Конструирование объекта Box");
        width = 10;
        height = 10;
        depth = 10;
    }

    // compute and return volume
    // рассчитать и возвратить объем
    double volume() {
        return width * height * depth;
    }
}

class BoxDemo6 {
    public static void main(String args[]) {
        // declare, allocate, and initialize Box objects
        // объявить, выделить память и инициализировать объекты типа
        Box mybox1 = new Box();
        Box mybox2 = new Box();

        double vol;

        // get volume of first box
        // получить объем первого параллелепипеда
        vol = mybox1.volume();
        System.out.println("Объём равен " + vol);

        // get volume of second box
        // получить объем второго параллелепипеда
        vol = mybox2.volume();
        System.out.println("Объём равен " + vol);
    }
}

