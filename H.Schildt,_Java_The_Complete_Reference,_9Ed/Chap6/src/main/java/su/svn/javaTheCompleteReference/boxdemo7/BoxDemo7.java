package su.svn.javaTheCompleteReference.boxdemo7;

/* Here, Box uses a parameterized constructor to
 * initialize the dimensions of a box.
 * В данном примере программы для инициализации размеров
 * параллелепипеда в классе Box применяется параме
 * тризированный конструктор.
 */

class Box {
    double width;
    double height;
    double depth;

    // This is the constructor for Box.
    // Это конструктор класса Box.
    Box(double w, double h, double d) {
        width = w;
        height = h;
        depth = d;
    }

    // compute and return volume
    // рассчитать и возвратить объем
    double volume() {
        return width * height * depth;
    }
}

class BoxDemo7 {
    public static void main(String args[]) {
        // declare, allocate, and initialize Box objects
        // объявить, выделить память и инициализировать объекты типа Box
        Box mybox1 = new Box(10, 20, 15);
        Box mybox2 = new Box(3, 6, 9);

        double vol;

        // get volume of first box
        // получить объем первого параллелепипеда
        vol = mybox1.volume();
        System.out.println("Volume is " + vol);

        // get volume of second box
        // получить объем второго параллелепипеда
        vol = mybox2.volume();
        System.out.println("Volume is " + vol);
    }
}

