package su.svn.javaTheCompleteReference.stack;

// This class defines an integer stack that can hold 10 values.
// В этом классе определяется целочисленный стек, в котором
// можно хранить до 10 целочисленных значений

class Stack {
    int stck[] = new int[10];
    int tos;

    // Initialize top-of-stack
    // инициализировать вершину стека
    Stack() {
        tos = -1;
    }

    // Push an item onto the stack
    // разместить элемент в стеке
    void push(int item) {
        if (tos == 9)
            // Stack is full.
            System.out.println("Стек заполнен.");
        else
            stck[++tos] = item;
    }

    // Pop an item from the stack
    // извлечь элемент из стека
    int pop() {
        if (tos < 0) {
            System.out.println("Стек не загружен.");
            return 0;
        } else
            return stck[tos--];
    }
}

