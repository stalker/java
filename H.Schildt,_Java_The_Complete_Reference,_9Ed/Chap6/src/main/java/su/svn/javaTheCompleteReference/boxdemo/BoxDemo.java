/*
 * A program that uses the su.svn.javaTheCompleteReference.boxdemo.Box class.
 * Call this file su.svn.javaTheCompleteReference.boxdemo.BoxDemo.java
 * Программа, использующая класс su.svn.javaTheCompleteReference.boxdemo.Box
 * Прис воить исх одному файлу имя su.svn.javaTheCompleteReference.boxdemo.BoxDemo.java.
 */

package su.svn.javaTheCompleteReference.boxdemo;

class Box {
    double width;
    double height;
    double depth;
}

// This class declares an object of type su.svn.javaTheCompleteReference.boxdemo.Box.
// В этом кла ссе объявляется объект типа su.svn.javaTheCompleteReference.boxdemo.Box.
class BoxDemo {
    public static void main(String args[]) {
        Box mybox = new Box();
        double vol;

        // assign values to mybox's instance variables
        // присвоить значение переменным экземпляра mybox
        mybox.width = 10;
        mybox.height = 20;
        mybox.depth = 15;

        // compute volume of box
        // рассчитать объем параллелепипеда
        vol = mybox.width * mybox.height * mybox.depth;

        // Volume is
        System.out.println("Объём равен " + vol);
    }
}
