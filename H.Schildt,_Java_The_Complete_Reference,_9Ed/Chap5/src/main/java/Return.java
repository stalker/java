/*
 * Demonstrate return.
 * Продемонстрировать применение оператора return.
 */

class Return {
    public static void main(String args[]) {
        boolean t = true;

        // Before the return.
        System.out.println("Дo возврата.");

        if (t) return; // возврат в вызывающий код // return to caller

        // This won't execute.
        System.out.println("Этот оператор выполняться не будет.");
    }
}
