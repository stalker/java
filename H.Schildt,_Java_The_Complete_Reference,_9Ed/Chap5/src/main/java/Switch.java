/*
 * An improved version of the season program.
 * Усовершенствованная версия программы, в которой
 * определяется принадлежность месяца времени года.
 */

class Switch {
    public static void main(String args[]) {
        int month = 4;
        String season;

        switch (month) {
          case 12:
          case 1:
          case 2:
            season = "эиме"; // Winter
            break;
          case 3:
          case 4:
          case 5:
            season = "весне"; // Spring
            break;
          case 6:
          case 7:
          case 8:
            season = "лету"; // Summer
            break;
          case 9:
          case 10:
          case 11:
            season = "осени"; // Autumn
            break;
          default:
            season = "вымышленным месяцам"; // Bogus Month
        }
        System.out.println("Aпpeль относится к " + season + "."); // April is in the
    }
}
