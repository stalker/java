/*
 * Demonstrate if-else-if statements.
 * Продемонстрировать применение конс трукции if-else-if.
 */

class IfElse {
    public static void main(String args[]) {
        int month = 4; // April // Апрель
        String season;

        if(month == 12 || month == 1 || month == 2)
          season = "зиме";  // Winter
        else if(month == 3 || month == 4 || month == 5)
          season = "весне"; // Spring
        else if(month == 6 || month == 7 || month == 8)
          season = "лету";  // Summer
        else if(month == 9 || month == 10 || month == 11)
          season = "осени"; // Autumn
        else
          season = "вымышленным месяцам"; // Bogus Month

        System.out.println("Aпpeль относится к " + season + "."); // April is in the
    }
}
