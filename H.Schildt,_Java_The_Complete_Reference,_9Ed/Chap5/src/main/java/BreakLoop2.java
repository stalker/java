/*
 * Using break to exit a while loop.
 * Применение оператора break для выхода из цикла while.
 */

class BreakLoop2 {
    public static void main(String args[]) {
        int i = 0;

        while (i < 100) {
            if (i == 10) break; // terminate loop if i is 10
                                // выход из цикла, если значение
                                // переменной i равно 10
            System.out.println("i: " + i);
            i++;
        }
        System.out.println("Цикл завершен."); // Loop complete.
    }
}
