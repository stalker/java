/*
 * Using break to exit a loop.
 * Применение оператора break для выхода из цикла.
 */

class BreakLoop {
    public static void main(String args[]) {
        for (int i = 0; i < 100; i++) {
            if (i == 10) break; // terminate loop if i is 10
                                // выход из цикла, если значение
                                // переменной i равно 10
            System.out.println("i: " + i);
        }
        System.out.println("Цикл завершен."); // Loop complete.
    }
}
