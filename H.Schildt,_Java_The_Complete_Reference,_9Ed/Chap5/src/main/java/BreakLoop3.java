/*
 * Using break with nested loops.
 * Применение оператора break во вложенных циклах.
 */

class BreakLoop3 {
    public static void main(String args[]) {
        for (int i = 0; i < 3; i++) {
            System.out.print("Pass " + i + ": ");
            for (int j = 0; j < 100; j++) {
                if (j == 10) break; // terminate loop if j is 10
                                    // выход из цикла, если значение
                                    // переменной j равно 10
                System.out.print(j + " ");
            }
            System.out.println();
        }
        System.out.println("Цикл завершен."); // Loop complete.
    }
}
