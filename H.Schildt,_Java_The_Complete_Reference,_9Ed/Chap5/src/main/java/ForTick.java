/*
 * Declare a loop control variable inside the for.
 * Объявить переменную управления циклом в самом цикле for.
 */

class ForTick {
    public static void main(String args[]) {

        // here, n is declared inside of the for loop
        // здесь переменная n объявляется в самом цикле for
        for (int n = 10; n > 0; n--)
            System.out.println("tick " + n);
    }
}
