/*
 * Using a do-while to process a menu selection -- a simple help system.
 * Использовать оператор цикла do-while для выбора пункта меню.
 */

class Menu {
    public static void main(String args[])
        throws java.io.IOException {
        char choice;

        do {
            System.out.println("Справка по оператору:");
            System.out.println("  1. if");
            System.out.println("  2. switch");
            System.out.println("  3. while");
            System.out.println("  4. do-while");
            System.out.println("  5. for\n");
            System.out.println("Выберете нужный пункт:");
            choice = (char) System.in.read();
        } while( choice < '1' || choice > '5');

        System.out.println("\n");

        switch(choice) {
          case '1':
            System.out.println("The if:\n");
            System.out.println("if(условие) оператор;");
            System.out.println("else оператор;");
            break;
          case '2':
            System.out.println("The switch:\n");
            System.out.println("switch(выражение) {");
            System.out.println("  case константа:");
            System.out.println("    последовательность операторов");
            System.out.println("  break;");
            System.out.println("  // ...");
            System.out.println("}");
            break;
          case '3':
            System.out.println("The while:\n");
            System.out.println("while(условие) оператор;");
            break;
          case '4':
            System.out.println("The do-while:\n");
            System.out.println("do {");
            System.out.println("  оператор;");
            System.out.println("} while (условие);");
            break;
          case '5':
            System.out.println("The for:\n");
            System.out.print("for(инициализация; условие; итерация)");
            System.out.println(" оператор;");
            break;
        }
    }
}
