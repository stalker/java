/*
 * The target of a loop can be empty.
 * Целевая часть цикла может быть пустой.
 */

class NoBody {
    public static void main(String args[]) {
        int i, j;

        i = 100;
        j = 200;

        // find midpoint between i and j
        // рассчитать среднее значе ние переменных i и j
        while(++i < --j) ; // no body in this loop
                           // у этого цикла отсутствует тело
        System.out.println("Midpoint is " + i);
    }
}
