/*
 * Using break to exit from nested loops.
 * Применение оператора break для выхода из вложенных циклов.
 */

class BreakLoop4 {
    public static void main(String args[]) {
        outer:
        for (int i = 0; i < 3; i++) {
            System.out.print("Pass " + i + ": ");
            for (int j = 0; j < 100; j++) {
                if (j == 10) break outer; // exit both loops
                                          // выход из обоих циклов
                System.out.print(j + " ");
            }
            // This will not print
            System.out.println("Этa строка не будет выводиться");
        }
        System.out.println("Цикл завершен."); // Loop complete.
    }
}
